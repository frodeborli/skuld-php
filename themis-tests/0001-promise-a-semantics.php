<?php

use Skuld\Promise;

require(__DIR__ . "/../vendor/autoload.php");

$promise = Promise::sleep(0)->then(function() {
    echo "1";
    return 2;
})->then(function($value) {
    echo $value;
    return 4; // 3 will be written by the finally() handler
});

$promise->finally(function() {
    echo "3";
});

$promise->then(function($value) {
    echo $value;
    throw new \Exception("OK");
})->then(function($value) {
    // This func should be bypassed
    echo "This shouldn't happen";
    assert(false, "This should not happen");
    throw new \Exception("WRONG");
})->catch(function($error) {
    assert($error->getMessage() === 'OK', "Wrong exception received");
    echo "\nALL IS " . $error->getMessage() . "\n";
    return "OK";
});

$promise->wait();

/**
 * This exception is caught, so it should not cause any problem
 */
(new Promise(function() {
    throw new Exception("NOT EXPECTING THIS TO BE LOGGED");
}))
->then(function($ignored) {})
->catch(function($handled) {
});

Themis::expectException(Exception::class, "Escaped exception");

/**
 * This exception is not caught, so it should be logged on output somehow
 */
$a = new Promise(function() {
    throw new Exception("Escaped exception");
});

$a->wait();
