<?php

use Skuld\Promise;

require(__DIR__ . "/../vendor/autoload.php");

$t = microtime(true);
Promise::sleep(1)->wait();
assert(microtime(true) - $t > 1, "Sleep took less than 1 second");