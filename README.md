Skuld
=====

A powerful generic promise implementation. API compatible with Promise/A+ from javascript
and interoperable with most other promise implementations.

Basic Usage
-----------

Promises can be resolved by calling either the `$fulfill` or `$reject` callbacks passed
to the resolve function. Alternatively the resolve function can return a value.

```php
<?php
use Skuld\Promise;

function alternative_1() {
    return new Promise(function($fulfill, $reject) {
        $fulfill("Future value");
    });
}

function alternative_2() {
    return new Promise(function() {
        return "Future value";
    })
}

function alternative_3() {
    return Promise::resolved("Future value");
}

alternative_1()->then(function($value) {
    echo $value . "\n";
});
```

Resolving promises
------------------

```php
<?php
use Skuld\Promise;

function future_value() {
    return new Promise(function() {
        return 10;
    });
}

echo future_value()->wait(); // outputs 10
```

Sleep promise
-------------

The `Promise::sleep(float $time)` function returns a Promise which will be resolved after `$time`
seconds. 

Example usage:

```php
<?php
use Skuld\Promise;

function value_in_10_second() {
    return new Promise(function() {
        Promise::sleep(10)->wait();
        return 123;
    });
}
```