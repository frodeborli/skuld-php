<?php
namespace Skuld;

use Closure;
use Fiber;
use FiberError;
use InvalidArgumentException;
use LogicException;
use Skuld\Promise\BoxedError;
use Skuld\Promise\State;
use ReflectionException;
use ReflectionMethod;
use RuntimeException;
use Throwable;
use WeakMap;

/**
 * A powerful Promise implementation.
 * 
 * @package Skuld
 */
class Promise implements Promiselike {

    /**
     * True when a shutdown handler has been registered for finishing
     * the event loop.
     * 
     * @var bool
     */
    private static bool $shutdownScheduled = false;

    /**
     * Tracks the previous start time for running events. The number of
     * event iterations per second is limited to 100 to avoid busy looping.
     * 
     * @var null|int
     */
    private static ?int $lastIterationTime = null;

    /**
     * A queue of Fiber instances with normal priority
     * 
     * @var Fiber[]
     */
    private static array $taskQueue = [];

    /**
     * Records the next available offset in the task queue.
     *  
     * @var int
     */
    private static int $taskQueueNextOffset = 0;

    /**
     * A queue of Fiber instances with high priority
     * 
     * @var Fiber[]
     */
    private static array $microtaskQueue = [];

    /**
     * Records the next available offset in the microtask queue.
     * 
     * @var int
     */
    private static int $microtaskQueueNextOffset = 0;

    /**
     * Stores the argument lists for fibers that haven't been started,
     * and the intermediate value for Fiber instances that are suspended.
     * 
     * @var WeakMap<Fiber, array{0: bool, 1: mixed}>
     */
    private static ?WeakMap $fiberVals = null;

    /**
     * Holds the error code for the most recent call to {@see self::promisify()}
     * 
     * @var null|int
     */
    public static ?int $lastPromisifyErrorCode = null;

    /**
     * Holds the reason why the call to {@see self::promisify()} failed.
     * 
     * @var null|string
     */
    public static ?string $lastPromisifyErrorMessage = null;

    /**
     * The current state of this promise
     * 
     * @var State
     */
    private State $state = State::PENDING;

    /**
     * The result value or error after this promise is either fulfilled
     * or rejected
     * 
     * @var mixed
     */
    private mixed $result = null;

    /**
     * Listeners which will be invoked if this promise is fulfilled
     * 
     * @var Closure[]
     */
    private array $onFulfillListeners = [];

    /**
     * Listeners which will be invoked if this promise is rejected
     * 
     * @var Closure[]
     */
    private array $onRejectListeners = [];

    /**
     * The promise returned when a listener has been added, to enable
     * chaining a sequence of promises: `$promise->then(xx)->then(xx)->then(xx)`
     * 
     * @var self[]
     */
    private array $nextPromises = [];

    /**
     * The first index where there are fulfill, reject and nextPromises.
     * 
     * @var int
     */
    private int $first = 0;

    /**
     * The index where future fulfill, reject and nextPromise values will be
     * inserted.
     * 
     * @var int
     */
    private int $last = 0;

    /**
     * Logs if the result of this promise has been forwarded to a callback
     * or to another promise.
     * 
     * @var bool
     */
    private bool $delivered = false;

    private string $traceLocation;

    /**
     * The resolver function may 
     * @param Closure|null $resolver 
     * @return void 
     */
    public function __construct(Closure $resolver=null) {
        $trace = \debug_backtrace(0, 10);
        foreach ($trace as $t) {
            if (!isset($t['file'])) {
                continue;
            }
            if ($t["file"] === __FILE__) {
                continue;
            }
            $this->traceLocation = $t['file'].':'.$t['line'];
        }
        if (!self::$shutdownScheduled) {
            self::scheduleShutdownHandler();
        }
        if ($resolver) {
            self::queueTask(static function($promise, $resolver) {
                try {
                    $result = $resolver($promise->fulfill(...), $promise->reject(...));
                    if ($promise->state === State::PENDING) {
                        $promise->fulfill($result);
                    } else {
                        throw new LogicException("The promise is already resolved");
                    }
                } catch (Throwable $error) {

                    if ($promise->state === State::PENDING) {
                        $promise->reject($error);
                    } else {
                        throw new LogicException("The promise is already resolved");
                    }
                }
            }, [ $this, $resolver ]);
        }
    }

    public function __debugInfo() {
        return [
            'result' => $this->result instanceof Throwable ? $this->result::class . ': ' . $this->result->getMessage() . ' in ' . $this->result->getFile() . ':' . $this->result->getLine() : $this->result,
            'state' => $this->state->value,
            'delivered' => $this->delivered,
        ];
    }

    public function __destruct() {
        if ($this->state->isPending() && (!empty($this->onFulfillListeners) || !empty($this->onRejectListeners))) {
            self::logError("Warning! Promise created at " . $this->traceLocation . " was not resolved, but has listeners attached");
            return;
        }
    }

    /**
     * Handle promise resolution.
     * 
     * @param Closure|null $onFulfill Invoked if the promise is fulfilled
     * @param Closure|null $onReject Invoked if the promise is rejected
     * @return Promise 
     */
    public function then($onFulfill=null, $onReject=null): self {
        if ($onFulfill === null && $onReject === null) {
            throw new InvalidArgumentException("At minimum one listener must be provided");
        }
        if ($onFulfill && !is_callable($onFulfill)) {
            throw new InvalidArgumentException("Argument 1 must be a callable");
        }
        if ($onReject) {
            if (!is_callable($onReject)) {
                throw new InvalidArgumentException("Argument 2 must be a callable");
            } else {
                $onReject = static function(BoxedError $error) use ($onReject) {
                    return $onReject($error->unbox());
                };
            }
        }

        $this->hasRejectionHandler = true;
        $this->onFulfillListeners[$this->last] = $onFulfill;
        $this->onRejectListeners[$this->last] = $onReject;
        $nextPromise = $this->nextPromises[$this->last++] = new self();
        if ($this->state->isResolved()) {
            $this->finalize();
        }
        return $nextPromise;
    }

    public function catch(Closure $onReject=null): self {
        return $this->then(null, $onReject);
    }

    public function finally(Closure $handler): self {
        $wrapper = function() use ($handler) {
            if ($this->state === State::REJECTED) {
                $this->result->unbox();
            }
            return $handler();
        };
        $this->onFulfillListeners[$this->last] = $wrapper;
        $this->onRejectListeners[$this->last] = $wrapper;
        $nextPromise = $this->nextPromises[$this->last++] = new self();
        $nextPromise->tag = 'Finally`s Next-Promise';
        if ($this->state->isResolved()) {
            $this->finalize();
        }
        return $nextPromise;
    }

    /**
     * Fulfill the promise and call any listeners that subscribe to this event.
     * 
     * @param mixed $result 
     * @return void 
     * @throws LogicException 
     */
    public function fulfill(mixed $result): void {
        if (self::promisify($result)) {
            die("HER");
            throw new \Exception("Fulfilled with a promise");
        }
        $this->assertPending();
        if ($promise = $this->promisify($result)) {
            $this->state = State::LIMBO;
            $promise->then(function($result) {
                if ($this->state === State::LIMBO) {
                    $this->state = State::PENDING;
                    $this->fulfill($result);
                } else {
                    throw new LogicException("The promise is no longer deferred while trying to fulfill it");
                }
            }, function($result) {
                if ($this->state === State::LIMBO) {
                    $this->state = State::PENDING;
                    $this->reject($result);
                } else {
                    throw new LogicException("The promise is no longer deferred while trying to reject it");
                }
            });
            return;
        } 
        $this->state = State::FULFILLED;
        $this->result = $result;
        $this->finalize();
    }

    /**
     * Reject the promise and call any listeners that subscribe to this event.
     * 
     * @param mixed $result 
     * @return void 
     * @throws LogicException 
     */
    public function reject(mixed $result): void {
        if (!($result instanceof BoxedError)) {
            $result = new BoxedError($result);
        }
        $this->assertPending();

        /**
         * If the rejection is a promise, we'll resolve that promise before passing
         * it to any rejection handlers.
         */
        if ($promise = $this->promisify($result)) {
            $this->state = State::LIMBO;
            $promise->then(function($result) {
                if ($this->state === State::LIMBO) {
                    $this->state = State::PENDING;
                    $this->reject($result);
                } else {
                    throw new LogicException("The promise is no longer deferred while trying to reject it");
                }
            }, function($newError) {
                if ($this->state === State::LIMBO) {
                    $this->state = State::PENDING;
                    $this->reject($newError);
                } else {
                    throw new LogicException("The promise is no longer deferred while trying to reject it");
                }
            });
        } else {
            $this->state = State::REJECTED;
            $this->result = $result;
            $this->finalize();    
        }
    }

    /**
     * If the promise is resolved, run any listeners.
     * 
     * @return void 
     */
    private function finalize(): void {
        if ($this->state === State::FULFILLED) {
            $listeners = &$this->onFulfillListeners;
            $discards = &$this->onRejectListeners;
        } elseif ($this->state === State::REJECTED) {
            $listeners = &$this->onRejectListeners;
            $discards = &$this->onFulfillListeners;
        } else {
            return;
        }
        while ($this->first < $this->last) {
            $listener = $listeners[$this->first];
            $nextPromise = $this->nextPromises[$this->first];
            unset($listeners[$this->first], $discards[$this->first], $this->nextPromises[$this->first]);
            ++$this->first;
            if ($listener === null) {
                if ($this->state === State::FULFILLED) {
                    $nextPromise->fulfill($this->result);
                } elseif ($this->state === State::REJECTED) {
                    $nextPromise->reject($this->result);
                }
            } else {
                self::queueMicrotask(function() use ($listener, $nextPromise) {
                    $this->delivered = true;
                    try {
                        $returnValue = $listener($this->result);
                        $nextPromise->fulfill($returnValue);
                    } catch (Throwable $error) {
                        $nextPromise->reject($error);
                    }
                });    
            }
        }
    }

    /**
     * Require that the promise is pending
     * 
     * @return void 
     * @throws LogicException 
     */
    private function assertPending(): void {
        if ($this->state !== State::PENDING) {
            throw new LogicException("Promise is already in the `" . $this->state->value . "` state");
        }
    }

    /**
     * Cast any promise to {@see \Skuld\Promise}. Uses reflection to validate
     * that the promise is a compatible implementation.
     * 
     * If casting fails the reason is available in the following properties:
     * 
     *  - {@see self::$lastPromisifyErrorMessage} holds a string message
     *  - {@see self::$lastPromisifyErrorCode} holds an integer error code
     * 
     * @param mixed $promiseLike 
     * @return null|Promise 
     */
    public static function promisify($promiseLike): ?Promise {
        self::$lastPromisifyErrorMessage = null;
        if ($promiseLike instanceof self) {
            return $promiseLike;
        }
        if (!\is_object($promiseLike)) {
            self::$lastPromisifyErrorCode = 7;
            self::$lastPromisifyErrorMessage = "Not an object";
            return null;
        }
        try {
            $rf = new ReflectionMethod($promiseLike, 'then');
            if ($rf->getNumberOfParameters() < 2) {
                self::$lastPromisifyErrorCode = 1;
                self::$lastPromisifyErrorMessage = "Method " . $promiseLike::class . "::then() doesn't accept at least two arguments";
                return null;
            }
            if (!$rf->isPublic()) {
                self::$lastPromisifyErrorCode = 2;
                self::$lastPromisifyErrorMessage = "Method " . $promiseLike::class . "::then() is not public";
                return null;
            }
            if ($rf->isStatic()) {
                self::$lastPromisifyErrorCode = 3;
                self::$lastPromisifyErrorMessage = "Method " . $promiseLike::class . "::then() is declared static";
                return null;
            }
        } catch (ReflectionException $e) {
            self::$lastPromisifyErrorCode = 4;
            self::$lastPromisifyErrorMessage = "Method " . $promiseLike::class . "::then() does not exist";
            return null;
        }
        $rps = $rf->getParameters();
        for ($i = 0; $i < 2; $i++) {
            if (!$rps[$i]->allowsNull()) {
                self::$lastPromisifyErrorCode = 5;
                self::$lastPromisifyErrorMessage = "Argument " . $rps[$i]->getName() . " of " . $promiseLike::class . "::then() is not optional";
                return null;
            }
            if ($rps[$i]->hasType()) {
                if (!\preg_match('/\b(callable|Closure)\b/', (string) $rps[$i]->getType())) {
                    self::$lastPromisifyErrorCode = 6;
                    self::$lastPromisifyErrorMessage = "Argument " . $rps[$i]->getName() . " of " . $promiseLike::class . "::then() only accepts the type `" . $rps[$i]->getType() . "` but it must allow Closure or callable";
                    return null;
                }
            }
        }

        $promise = new self();
        $promiseLike->then($promise->fulfill(...), $promise->reject(...));
        return $promise;
    }

    public static function all(object|array ...$promises): Promise {
        if (count($promises) === 1) {
            if (is_array($promises[0])) {
                return static::all($promises[0]);
            } elseif ($castPromise = static::promisify($promises[0])) {
                $list = [ $castPromise ];
            } else {
                throw new LogicException("Promise::all() takes only promises and arrays of promises");
            }
        } else {
            $list = [];
            foreach ($promises as $promise) {
                if (\is_array($promise)) {
                    $list[] = static::all($promise);
                } elseif ($castPromise = static::promisify($promise)) {
                    $list[] = $castPromise;
                } else {
                    throw new LogicException("Promise::all() takes only promises and arrays of promises");
                }
            }
        }

        return new self(function() use ($list) {
            $results = [];
            foreach ($list as $promise) {
                try {
                    $results[] = $promise->wait();
                } catch (Throwable $error) {
                    $results[] = $error;
                }
            }
            return $results;
        });
    }

    /**
     * @deprecated Use {@see Promise::fulfilled()} instead
     * @param mixed $result 
     * @return Promise 
     */
    public static function resolved(mixed $result): Promise {
        return self::fulfilled($result);
    }

    /**
     * Returns a fulfilled promise
     * 
     * @param mixed $result 
     * @return Promise 
     * @throws LogicException 
     */
    public static function fulfilled(mixed $result): Promise {
        $promise = new self();
        $promise->result = $result;
        $promise->state = State::FULFILLED;
        return $promise;
    }

    /**
     * Returns a rejected promise
     * 
     * @param Throwable $result 
     * @return Promise 
     * @throws LogicException 
     */
    public static function rejected(mixed $result): Promise {
        $promise = new self();
        $promise->result = new BoxedError($result);
        $promise->state = State::REJECTED;
        return $promise;
    }

    /**
     * Returns a promise which will be resolved after a number of
     * seconds.
     * 
     * @todo There should be special handling of sleep promises in the event loop
     * @param float $time Number of seconds before resolving
     * @return Promise 
     */
    public static function sleep(float $time): Promise {
        $start = \hrtime(true);
        $expires = $start + (int) ($time * 1_000_000_000);
        $promise = new self(function($fulfill, $reject) use ($expires, $start) {
            while ($expires > \hrtime(true)) {
                Fiber::suspend();
            }
            /**
             * Return actually elapsed time
             */
            return (\hrtime(true) - $start) / 1_000_000_000;
        });
        return $promise;
    }

    /**
     * Wait synchronously for the promise to become resolved
     * 
     * @param mixed $promises 
     * @return mixed 
     * @throws LogicException 
     * @throws FiberError 
     * @throws Throwable 
     * @throws RuntimeException 
     */
    public function wait(): mixed {
        if (Fiber::getCurrent()) {
            $result = Fiber::suspend($this);
            if (self::promisify($result)) {
                throw new LogicException("Promise should be resolved by Fiber::suspend()");
            }
            return $result;
        } else {
            while ($this->state === State::PENDING || $this->state === State::LIMBO) {
                if (self::runTasks() === 0) {
                    throw new LogicException("The event queue is empty, and this promise is still pending");
                }
            }
            if ($this->state === State::FULFILLED) {
                return $this->result;
            } elseif ($this->state === State::REJECTED) {
                $error = $this->result->unbox();
                if ($error instanceof Throwable) {
                    throw $error;
                } else {
                    throw new RuntimeException("Promise was rejected by an unthrowable value of type " . \get_debug_type($this->result));
                }
                throw $this->result->unbox();
            } else {
                throw new RuntimeException("Promise is neither fulfilled nor rejected");
            }
        }    
    }

    /**
     * Schedule a microtask to run.
     * 
     * @param Closure $closure 
     * @param array $args 
     * @return void 
     */
    private static function queueMicrotask(Closure $closure, array $args=[]): void {
        if (self::$fiberVals === null) {
            self::$fiberVals = new WeakMap();
        }
        $fiber = new Fiber($closure);
        self::$fiberVals[$fiber] = $args;
        self::$microtaskQueue[self::$microtaskQueueNextOffset++] = $fiber;
    }

    private static function queueTask(Closure $closure, array $args=[]): void {
        if (self::$fiberVals === null) {
            self::$fiberVals = new WeakMap();
        }
        $fiber = new Fiber($closure);
        self::$fiberVals[$fiber] = $args;
        self::$taskQueue[self::$taskQueueNextOffset++] = $fiber;
    }

    /**
     * Run the Fiber instance one iteration. Returns true if the fiber has more work
     * to do
     * 
     * @param Fiber $fiber 
     * @return bool 
     */
    private static function runFiberStep(Fiber $fiber, Closure $requeue): void {
        try {
            if (!$fiber->isStarted()) {
                self::$fiberVals[$fiber] = [ false, $fiber->start(...self::$fiberVals[$fiber]) ];
            } elseif ($fiber->isSuspended()) {
                [ $throwException, $value ] = self::$fiberVals[$fiber];
                if ($throwException) {
                    self::$fiberVals[$fiber][0] = false;
                    self::$fiberVals[$fiber][1] = $fiber->throw($value);
                } else {
                    self::$fiberVals[$fiber][1] = $fiber->resume($value);
                }
            } elseif ($fiber->isTerminated()) {
                throw new LogicException("A terminated fiber was found in the event loop");
            } else {
                throw new LogicException("Unknown fiber status");
            }

            if ($promise = self::promisify(self::$fiberVals[$fiber][1])) {
                $promise->then(function($result) use ($fiber, $requeue) {
                    self::$fiberVals[$fiber][1] = $result;
                    $requeue($fiber);
                }, function($error) use ($fiber, $requeue) {
                    self::$fiberVals[$fiber][0] = true;
                    self::$fiberVals[$fiber][1] = $error;
                    $requeue($fiber);
                });
            } elseif ($fiber->isSuspended()) {
                $requeue($fiber);
            }

        } catch (Throwable $error) {
            self::logException($error);
        }
    }

    /**
     * Run all pending microtasks and also any new microtasks that are being
     * added.
     * 
     * @return int 
     */
    private static function runMicrotasks(): int {
        if (Fiber::getCurrent()) {
            self::logException(new LogicException("Can't run microtasks from within a Fiber"));
            die();
        }
        $count = 0;
        $index = \array_key_first(self::$microtaskQueue);
        do {
            if (isset(self::$microtaskQueue[$index])) {
                $fiber = self::$microtaskQueue[$index];
                unset(self::$microtaskQueue[$index]);
                ++$count;
                self::runFiberStep($fiber, static function(Fiber $fiber) use (&$index) {
                    if ($index === self::$microtaskQueueNextOffset - 1) {
                        self::logError("Microtask demoted to task");
                        self::$taskQueue[self::$taskQueueNextOffset++] = $fiber;
                    } else {
                        self::$microtaskQueue[self::$microtaskQueueNextOffset++] = $fiber;
                    }
                });
            }
        } while ($index++ < self::$microtaskQueueNextOffset);

        return $count;        
    }

    /**
     * Run any pending tasks, but don't run any tasks that are added while running.
     * 
     * @return int 
     */
    private static function runTasks(): int {
        if (Fiber::getCurrent()) {
            self::logException(new LogicException("Can't run tasks from within a Fiber"));
            die();
        }
        if (empty(self::$taskQueue) && empty(self::$microtaskQueue)) {
            return 0;
        }
        try {
            if (self::$lastIterationTime === null) {
                self::$lastIterationTime = \hrtime(true);
            } else {
                $startTime = \hrtime(true);
                $elapsed = $startTime - self::$lastIterationTime;
                
                if ($elapsed < 1_000_000) {
                    /**
                     * Previous iteration was run 1/1000th of a second ago, which
                     * seems like a busy loop - so we'll just yield some CPU time
                     * to avoid destroying the planet.
                     */
                    \usleep(1);
                }
                self::$lastIterationTime = $startTime;
            }
            $count = self::runMicrotasks();
            $stopIndex = self::$taskQueueNextOffset;
            $index = \array_key_first(self::$taskQueue);
            do {
                if (isset(self::$taskQueue[$index])) {
                    $fiber = self::$taskQueue[$index];
                    unset(self::$taskQueue[$index]);
                    self::runFiberStep($fiber, static function(Fiber $fiber) {
                        self::$taskQueue[self::$taskQueueNextOffset++] = $fiber;
                    });
                    $count += 1 + self::runMicrotasks();
                }
            } while ($index++ < $stopIndex);
    
            return $count;    
        } finally {
        }
    }

    private static function drainQueues(): void {
        while (!empty(self::$taskQueue) || !empty(self::$microtaskQueue)) {
            self::runTasks();
        }
    }

    /**
     * Log an exception to STDERR.
     * 
     * @param Throwable $error 
     * @return void 
     */
    private static function logException(Throwable $error): void {
        throw $error;
        if (Fiber::getCurrent()) {
            Fiber::suspend($error);
        }
        self::logError((string) $error);//"Unhandled " . $error::class . ":\n" . $error->getMessage() . " thrown in " . $error->getFile() . ":" . $error->getLine() . "\n" . $error->getTraceAsString());
    }

    /**
     * Log errors to STDERR.
     * 
     * @param string $message 
     * @return void 
     */
    private static function logError(string $message): void {
        fwrite(\STDERR, date('Y-m-d H:i:s') . ' ' . \rtrim($message)."\n");
    }

    private static function scheduleShutdownHandler(): void {
        self::$shutdownScheduled = true;
        \register_shutdown_function(self::drainQueues(...));
    }
}

