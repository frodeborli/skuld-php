<?php
namespace Skuld;

/**
 * A generic interface for objects that are promises
 * @package RaceTrack
 */
interface Promiselike {
    public function then(callable $onFulfilled=null, callable $onRejected=null);
}