<?php
namespace Skuld\Promise;

/**
 * Enumerates the various states a promise can be in
 */
enum State : string {
    /**
     * The promise is unresolved.
     */
    case PENDING = 'pending';

    /**
     * The promise is being resolved by another promise. It is neither pending
     * nor fulfilled.
     */
    case LIMBO = 'limbo';

    /**
     * The promise was fulfilled
     */
    case FULFILLED = 'fulfilled';

    /**
     * The promise was rejected
     */
    case REJECTED = 'rejected';

    public function isResolved(): bool {
        return $this === self::FULFILLED || $this === self::REJECTED;
    }

    public function isPending(): bool {
        return $this === self::PENDING || $this === self::LIMBO;
    }
}
