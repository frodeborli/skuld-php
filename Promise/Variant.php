<?php
namespace Skuld\Promise;

/**
 * Identifies the promise variant.
 * 
 * @internal
 */
enum Variant: string {
    case NORMAL = 'Promise';
    case THEN = 'then(): Promise';
    case FULFILLED = 'fulfilled(): Promise';
    case REJECTED = 'rejected(): Promise';
    case ALL = 'all(): Promise';
    case CATCH = 'catch(): Promise';
    case FINALLY = 'finally(): Promise';
    case PROMISIFIED = 'promisify(): Promise';
    case SLEEP = 'sleep(): Promise';
}