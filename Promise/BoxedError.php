<?php
namespace Skuld\Promise;

use RuntimeException;
use Throwable;

/**
 * Boxes errors from rejected promises, to ensure that the error
 * is caught or logged.
 * 
 * @package Skuld\Promise
 */
final class BoxedError {
    
    private bool $unboxed = false;
    private mixed $error;

    public function __construct(mixed $error) {
        $this->error = $error;
    }

    public function unbox(): mixed {
        $this->unboxed = true;
        return $this->error;
    }

    public function __destruct() {
        if (!$this->unboxed) {
            if ($this->error instanceof Throwable) {
                throw $this->error;
            } else {
                throw new RuntimeException("Unhandled rejection of type " . \get_debug_type($this->error) . " in promise");
            }
        }
    }
}